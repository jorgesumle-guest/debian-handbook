msgid ""
msgstr ""
"Project-Id-Version: 0\n"
"POT-Creation-Date: 2016-04-10 09:00+0900\n"
"PO-Revision-Date: 2017-07-05 12:30+0000\n"
"Last-Translator: Ingrid Yrvin <ingrid.yrvin@gmail.com>\n"
"Language-Team: Norwegian Bokmål <https://hosted.weblate.org/projects/debian-handbook/70_conclusion/nb_NO/>\n"
"Language: nb-NO\n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-publican; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 2.16-dev\n"

msgid "Future"
msgstr "Fremtiden"

msgid "Improvements"
msgstr "Forbedringer"

msgid "Opinions"
msgstr "Meninger"

msgid "Conclusion: Debian's Future"
msgstr "Konklusjon: Debians fremtid"

msgid "The story of Falcot Corp ends with this last chapter; but Debian lives on, and the future will certainly bring many interesting surprises."
msgstr "Historien om Falcot-selskapet tar slutt i det siste kapittel; men Debian lever videre, og fremtiden vil helt klart gi mange interessante overraskelser."

msgid "Upcoming Developments"
msgstr "Kommende utviklinger"

msgid "Weeks (or months) before a new version of Debian is released, the Release Manager picks the codename for the next version. Now that Debian version 8 is out, the developers are already busy working on the next version, codenamed <emphasis role=\"distribution\">Stretch</emphasis>…"
msgstr "Uker (eller måneder) før en ny versjon av Debian gis ut, velger utgivelsesadministratorene kodenavn for neste versjon. Nå når Debian versjon 8 er ute, er utviklerne allerede travelt opptatt med å jobbe på neste versjon, med kodenavn <emphasis role=\"distribution\">Stretch</emphasis>…"

msgid "There is no official list of planned changes, and Debian never makes promises relating to technical goals of the coming versions. However, a few development trends can already be noted, and we can try some bets on what might happen (or not)."
msgstr "Det finnes ingen offisiell liste med planlagte endringer, og Debian gir aldri lovnader relatert til tekniske mål for de neste versjonene.  Men det er en del utviklingstrender en allerede nå kan merke seg, og vi kan forsøke oss på noen spådommer om hva som kan skje (eller ikke)."

msgid "In order to improve security and trust, most if not all the packages will be made to build reproducibly; that is to say, it will be possible to rebuild byte-for-byte identical binary packages from the source packages, thus allowing everyone to verify that no tampering has happened during the builds."
msgstr "For å kunne forbedre sikkerhet og tillit vil de fleste (om ikke alle) pakkene endres til å ha reproduserbare bygg, dvs. det vil være mulig å bygge pakken på nytt og få identiske binærpakker fra kildekodepakkene.  Det vil gjøre det mulig for hvem som helst å kontrollere at ingen manipulering har skjedd under bygging."

msgid "In a related theme, a lot of effort will have gone into improving security by default, and mitigating both “traditional” attacks and the new threats implied by mass surveillance."
msgstr "I et relatert tema har det vært gjort mye innsats i å forbedre sikkerheten i standardoppsettet, og å gjøre både «tradisjonelle» angrep og de nye truslene som kommer fra masseovervåkning vanskeligere."

msgid "Of course, all the main software suites will have had a major release. The latest version of the various desktops will bring better usability and new features. Wayland, the new display server that is being developed to replace X11 with a more modern alternative, will be available (although maybe not default) for at least some desktop environments."
msgstr "Alle de store programvarepakkene vil, naturligvis, ha fått nye hovedutgaver.  Siste versjon av ulike skrivebordsmiljøer vil gi bedre brukeropplevelse og nye egenskaper. Wayland, den nye grafiske skjermtjeneren som utvikles for å bytte ut X11 med et mer moderne alternativ vil være tilgjengelig (men kanskje ikke tatt i bruk som standard) i hvert fall for noen skrivebordsmiljøer."

msgid "A new feature of the archive maintenance software, “bikesheds”, will allow developers to host special-purpose package repositories in addition to the main repositories; this will allow for personal package repositories, repositories for software not ready to go into the main archive, repositories for software that has only a very small audience, temporary repositories for testing new ideas, and so on."
msgstr "En ny funksjon i arkivet for å vedlikeholde programvare, «bikesheds», tillater utviklere å være vertskap for pakkebrønner for spesielle formål i tillegg til hovedkatalogene. Det vil gi rom for personlige pakkekbrønner, pakkebrønner med programvare som ikke er klare for hovedarkivet, pakkebrønner med programvare som bare har et svært lite publikum, midlertidige pakkebrønner til å teste nye ideer, og så videre."

msgid "Debian's Future"
msgstr "Debians fremtid"

msgid "In addition to these internal developments, one can reasonably expect new Debian-based distributions to come to light, as many tools keep making this task easier. New specialized subprojects will also be started, in order to widen Debian's reach to new horizons."
msgstr "I tillegg til denne internutviklingen så kan en med rimelighet forvente at nye Debian-baserte distribusjoner vil dukke opp, da flere verktøy fortsetter å gjøre det stadig enklere. Nye spesialiserte underprosjekter vil bli opprettet, for å øke Debians rekkevidde mot nye horisonter."

msgid "The Debian user community will increase, and new contributors will join the project… including, maybe, you!"
msgstr "Debians brukerfellesskap vil øke, og nye bidragsytere vil delta i prosjektet ... medregnet, kanskje deg!"

msgid "The Debian project is stronger than ever, and well on its way towards its goal of a universal distribution; the inside joke within the Debian community is about <foreignphrase>World Domination</foreignphrase>."
msgstr "Debian-prosjektet er sterkere enn noensinne, og godt på vei mot målet om en universell distribusjon. Internspøken i Debian-fellesskapet er <foreignphrase>Verdensherredømme</foreignphrase>."

msgid "In spite of its old age and its respectable size, Debian keeps on growing in all kinds of (sometimes unexpected) directions. Contributors are teeming with ideas, and discussions on development mailing lists, even when they look like bickerings, keep increasing the momentum. Debian is sometimes compared to a black hole, of such density that any new free software project is attracted."
msgstr "Til tross for sin alder og sin respektable størrelse, holder Debian på å vokse i alle mulige (noen ganger uventede) retninger. Bidragsytere er fulle av ideer, og diskusjoner på e-postlistene for utvikling. Selv om det ser ut som krangling, viser de økt momentum. Noen ganger sammenlignes Debian med et svart hull med en slik tetthet at ethvert nytt fri programvare-prosjekt er tiltrukket av det."

msgid "Beyond the apparent satisfaction of most Debian users, a deep trend is becoming more and more indisputable: people are increasingly realising that collaborating, rather than working alone in their corner, leads to better results for everyone. Such is the rationale used by distributions merging into Debian by way of subprojects."
msgstr "I tillegg til at de fleste brukerne av Debian tilsynelatende er tilfredse, så blir dypere trend mer og mer synlig: folk innser i økende grad at samarbeid, snarere enn å jobbe alene i hvert sitt hjørne, fører til bedre resultater for alle. Det er begrunnelsen som kommer fra distribusjoner som går inn i Debian som delprosjekter."

msgid "The Debian project is therefore not threatened by extinction…"
msgstr "Debian-prosjektet er dermed ikke utryddingstruet …"

msgid "Future of this Book"
msgstr "Denne bokens fremtid"

msgid "We would like this book to evolve in the spirit of free software. We therefore welcome contributions, remarks, suggestions, and criticism. Please direct them to Raphaël (<email>hertzog@debian.org</email>) or Roland (<email>lolando@debian.org</email>). For actionable feedback, feel free to open bug reports against the <literal>debian-handbook</literal> Debian package. The website will be used to gather all information relevant to its evolution, and you will find there information on how to contribute, in particular if you want to translate this book to make it available to an even larger public than today. <ulink type=\"block\" url=\"http://debian-handbook.info/\" />"
msgstr "Vi ønsker at denne boken skal utvikle seg i tråd med ånden til fri programvare. Derfor ønsker vi velkommen bidrag, kommentarer, forslag og kritkk.  Det er veldig fint om du kan sende dem til Raphaël (<email>hertzog@debian.org</email>), eller Roland (<email>lolando@debian.org</email>). For konkrete tilbakemeldinger der noe kan fikses, lag gjerne en feilrapport knyttet til Debian-pakken <literal>debian-handbook</literal>. Nettstedet vil brukes til å samle all relevant informasjon om bokens utvikling, og du vil der finne informasjon om hvordan du kan bidra, særlig hvis du ønsker å oversette boken for å gjøre den tilgjengelig for enda større folkemengder enn i dag. <ulink type=\"block\" url=\"http://debian-handbook.info/\" />"

msgid "We tried to integrate most of what our experience at Debian taught us, so that anyone can use this distribution and take the best advantage of it as soon as possible. We hope this book contributes to making Debian less confusing and more popular, and we welcome publicity around it!"
msgstr "Vi har forsøkt å ta med det meste av hva vår Debian-erfaring har lært oss, slik at alle kan bruke denne distribusjonen, og utnytte de beste fordelene så raskt som mulig. Vi håper denne boken bidrar til å gjøre Debian mindre forvirrende og mer populær, og ønsker all oppmerksomhet om boken velkommen!"

msgid "We would like to conclude on a personal note. Writing (and translating) this book took a considerable amount of time out of our usual professional activity. Since we are both freelance consultants, any new source of income grants us the freedom to spend more time improving Debian; we hope this book to be successful and to contribute to this. In the meantime, feel free to retain our services! <ulink type=\"block\" url=\"http://www.freexian.com\" /> <ulink type=\"block\" url=\"http://www.gnurandal.com\" />"
msgstr "Vi ønsker å avslutte med enn personlig merknad. Skriving (og oversettelse) av denne boken tok betydelig andel tid fra vår vanlige faglige aktivitet. Siden vi er begge freelance-konsulenter, gir eventuelle nye inntektskilder oss frihet til å bruke mer tid på å forbedre Debian: Vi håper denne boken vil være vellykket og å bidra til dette. I mellomtiden, ta gjerne i bruk gjerne våre tjenester! <ulink type=\"block\" url=\"http://www.freexian.com\" /> <ulink type=\"block\" url=\"http://www.gnurandal.com\" />"

msgid "See you soon!"
msgstr "Vi sees!"
