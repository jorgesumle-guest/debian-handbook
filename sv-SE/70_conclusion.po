msgid ""
msgstr ""
"Project-Id-Version: 0\n"
"POT-Creation-Date: 2015-10-19 15:00+0200\n"
"PO-Revision-Date: 2017-12-23 02:50+0000\n"
"Last-Translator: Sebastian Rasmussen <sebras@gmail.com>\n"
"Language-Team: Swedish <https://hosted.weblate.org/projects/debian-handbook/70_conclusion/sv/>\n"
"Language: sv-SE\n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-publican; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 2.19-dev\n"

msgid "Future"
msgstr "Framtid"

msgid "Improvements"
msgstr "Förbättringar"

msgid "Opinions"
msgstr "Åsikter"

msgid "Conclusion: Debian's Future"
msgstr "Slutsats: Debians framtid"

msgid "The story of Falcot Corp ends with this last chapter; but Debian lives on, and the future will certainly bring many interesting surprises."
msgstr "Historien om Falcot Corp tar slut med detta sista kapitel, men Debian lever vidare, och framtiden kommer säkerligen medföra många spännande överraskningar."

msgid "Upcoming Developments"
msgstr "Kommande utveckling"

msgid "Weeks (or months) before a new version of Debian is released, the Release Manager picks the codename for the next version. Now that Debian version 8 is out, the developers are already busy working on the next version, codenamed <emphasis role=\"distribution\">Stretch</emphasis>…"
msgstr "Veckor (eller månader) innan en ny utgåva av Debian släpps väljer den utgivningsansvariga kodnamnet för nästa version. Nu då version 8 av Debian har släppts så är utvecklarna redan i full färd med att arbeta på nästa version, med kodnamnet <emphasis role=\"distribution\">Stretch</emphasis>…"

msgid "There is no official list of planned changes, and Debian never makes promises relating to technical goals of the coming versions. However, a few development trends can already be noted, and we can try some bets on what might happen (or not)."
msgstr "Det finns ingen officiell lista över planerade ändringar, och Debian avlägger aldrig några löften om tekniska mål för kommande versioner. Några utvecklingstrender kan dock redan ses, och vi kan komma med vissa gissningar om vad som kanske kommer (eller inte)."

msgid "In order to improve security and trust, most if not all the packages will be made to build reproducibly; that is to say, it will be possible to rebuild byte-for-byte identical binary packages from the source packages, thus allowing everyone to verify that no tampering has happened during the builds."
msgstr "För ökad säkerhet och tillit kommer de flesta, om inte alla, paketen att göras så att de kan byggas reproducerbart. Det vill säga att det kommer vara möjligt att upprepat bygga binära paket som är identiska på bitnivå från källpaketen, vilket tillåter var och en att bekräfta att ingen manipulering har skett under byggandet."

msgid "In a related theme, a lot of effort will have gone into improving security by default, and mitigating both “traditional” attacks and the new threats implied by mass surveillance."
msgstr "Relaterat till detta kommer mycket möda att ha ägnats åt att förbättra standardsystemets säkerhet, och mildra effekterna av både ”traditionella” attacker samt de nya hot som härstammar från massövervakning."

msgid "Of course, all the main software suites will have had a major release. The latest version of the various desktops will bring better usability and new features. Wayland, the new display server that is being developed to replace X11 with a more modern alternative, will be available (although maybe not default) for at least some desktop environments."
msgstr "Självklart kommer alla de stora programvarusviterna ha släppt en ny utgåva. Den senaste versionen av de olika skrivbordsmiljöerna kommer ge ökad användbarhet och nya funktioner. Wayland, den nya displayservern som utvecklas för att ersätta X11 med ett mer modernt alternativ, kommer att vara tillgänglig (om än kanske inte standardvalet) för åtminstone några skrivbordsmiljöer."

msgid "A new feature of the archive maintenance software, “bikesheds”, will allow developers to host special-purpose package repositories in addition to the main repositories; this will allow for personal package repositories, repositories for software not ready to go into the main archive, repositories for software that has only a very small audience, temporary repositories for testing new ideas, and so on."
msgstr "En ny funktion för arkivhanteringsprogramvaran, ”bikesheds” (cykelskjul), kommer att låta utvecklare utöver huvudförråden att även ha paketförråd för speciella syften. Detta kommer möjliggöra personliga paketförråd, förråd för programvara som inte ännu är redo att föras över till huvudarkivet, förråd för programvara som bara har en väldigt liten skara användare, tillfälliga förråd för att testa nya idéer och så vidare."

msgid "Debian's Future"
msgstr "Debians framtid"

msgid "In addition to these internal developments, one can reasonably expect new Debian-based distributions to come to light, as many tools keep making this task easier. New specialized subprojects will also be started, in order to widen Debian's reach to new horizons."
msgstr "Utöver denna interna utveckling är det rimligt att förvänta sig att nya Debian-baserade distributioner kommer att se dagens ljus, då många verktyg fortsätter att göra denna uppgift enklare. Nya specialiserade underprojekt kommer också att startas, för att kunna bredda Debian så att det når nya horisonter."

msgid "The Debian user community will increase, and new contributors will join the project… including, maybe, you!"
msgstr "Debians användargemenskap kommer att bli större, och nya bidragsgivare kommer att ansluta sig till projektet… kanske även innefattande dig!"

msgid "The Debian project is stronger than ever, and well on its way towards its goal of a universal distribution; the inside joke within the Debian community is about <foreignphrase>World Domination</foreignphrase>."
msgstr "Debian-projektet är starkare än någonsin, och på god väg att nå sitt mål om att vara en universell distribution, ett internskämt inom Debian-gemenskapen är att tala om <foreignphrase>världsherravälde</foreignphrase>."

msgid "In spite of its old age and its respectable size, Debian keeps on growing in all kinds of (sometimes unexpected) directions. Contributors are teeming with ideas, and discussions on development mailing lists, even when they look like bickerings, keep increasing the momentum. Debian is sometimes compared to a black hole, of such density that any new free software project is attracted."
msgstr "Trots sin höga ålder och sin respektingivande storlek fortsätter Debian att växa i alla sorters (ibland oväntade) riktningar. Bidragsgivare har ett överflöd av idéer, och diskussioner på sändlistor för utveckling bidrar, även då de verkar vara småkäbbel, till att föra dessa framåt. Debian liknas ibland med ett svart hål, vilket har sådan massa att det attraherar alla nya projekt inom fri programvara."

msgid "Beyond the apparent satisfaction of most Debian users, a deep trend is becoming more and more indisputable: people are increasingly realising that collaborating, rather than working alone in their corner, leads to better results for everyone. Such is the rationale used by distributions merging into Debian by way of subprojects."
msgstr "Förutom den tydliga nöjdheten hos de flesta Debian-användarna håller en djup trend på att bli alltmer obestridlig: folk inser i allt större grad att samarbete, snarare än att arbeta ensam på sin kant, leder till bättre resultat för alla. Detta är den logiska grund som används av distributioner som sammanförs med Debian genom underprojekt."

msgid "The Debian project is therefore not threatened by extinction…"
msgstr "Debian-projektet är alltså inte hotat av undergång…"

msgid "Future of this Book"
msgstr "Framtiden för denna bok"

msgid "We would like this book to evolve in the spirit of free software. We therefore welcome contributions, remarks, suggestions, and criticism. Please direct them to Raphaël (<email>hertzog@debian.org</email>) or Roland (<email>lolando@debian.org</email>). For actionable feedback, feel free to open bug reports against the <literal>debian-handbook</literal> Debian package. The website will be used to gather all information relevant to its evolution, and you will find there information on how to contribute, in particular if you want to translate this book to make it available to an even larger public than today. <ulink type=\"block\" url=\"http://debian-handbook.info/\" />"
msgstr "Vi vill att denna bok utvecklas enligt andemeningen för fri programvara. Vi välkomnar därför bidrag, påpekanden, förslag och kritik. Skicka dem till Raphaël (<email>hertzog@debian.org</email>) eller Roland (<email>lolando@debian.org</email>). För återkoppling som går att åtgärda är du välkommen att öppna felrapporter mot Debian-paketet <literal>debian-handbook</literal>. Webbplatsen kommer att användas för att samla all information som är relevant för dess utveckling, och där hittar du information om hur du kan bidra, i synnerhet om du vill översätta denna bok och göra den tillgänglig för en ännu större publik än idag. <ulink type=\"block\" url=\"http://debian-handbook.info/\" />"

msgid "We tried to integrate most of what our experience at Debian taught us, so that anyone can use this distribution and take the best advantage of it as soon as possible. We hope this book contributes to making Debian less confusing and more popular, and we welcome publicity around it!"
msgstr "Vi har försökt införliva det mesta som vår erfarenhet med Debian har lärt oss, så att alla ska kunna använda denna distribution och utnyttja den på bästa sätt så snabbt som möjligt. Vi hoppas att denna bok bidrar till att göra Debian mindre förvirrande och mer populärt, och vi välkomnar publicitet kring den!"

msgid "We would like to conclude on a personal note. Writing (and translating) this book took a considerable amount of time out of our usual professional activity. Since we are both freelance consultants, any new source of income grants us the freedom to spend more time improving Debian; we hope this book to be successful and to contribute to this. In the meantime, feel free to retain our services! <ulink type=\"block\" url=\"http://www.freexian.com\" /> <ulink type=\"block\" url=\"http://www.gnurandal.com\" />"
msgstr "Slutligen vill vi avsluta med en personlig notis. Att skriva (och översätta) denna bok tog en betydande tid i anspråk från vår vanliga yrkesaktivitet. Eftersom vi båda är frilanskonsulter så förunnar alla nya inkomstkällor oss friheten att tillbringa mer tid med att förbättra Debian, vi hoppas att denna bok kommer att lyckas och kan bidra till detta. Under tiden är ni välkomna att anlita oss! <ulink type=\"block\" url=\"http://www.freexian.com\" /> <ulink type=\"block\" url=\"http://www.gnurandal.com\" />"

msgid "See you soon!"
msgstr "Vi ses snart!"
